package bactrian

import "testing"

func assertEquals(actual interface{}, expected interface{},  t *testing.T) {
	if expected!= actual{
		t.Errorf("Expected '%v', got '%v'", expected, actual)
	}
}
