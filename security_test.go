package bactrian

import (
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"testing"
)

func TestHash(t *testing.T) {
	if bcrypt.CompareHashAndPassword([]byte(Hash("test")), []byte("test")) != nil {
		t.Errorf("Unable to generate hash of the string \"test\"!")
	}
}

func TestBadHash(t *testing.T) {
	if bcrypt.CompareHashAndPassword([]byte(Hash("test!")), []byte("test")) == nil {
		t.Errorf("Hashing validation failed!")
	}
}

func TestRequest(t *testing.T) {
	context, _ := gin.CreateTestContext(writer{})
	assertEquals(context.Writer.Status(), http.StatusOK, t)
}

func TestDenyRequest(t *testing.T) {
	context := mockContext()
	denyRequest(context)
	assertEquals(context.Writer.Status(), http.StatusUnauthorized, t)
}

