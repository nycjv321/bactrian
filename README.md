# Bactrian

basic auth middleware for Gin. 

## Usage

    engine.Use(bactrian.BasicAuth(users))
    
### Example UserService 

    import (
    	"os"
    	"strings"
    	"gitlab.com/nycjv321/bactrian"
    	"golang.org/x/crypto/bcrypt"
    )
    
    
    type AuthenticatedUserService struct {
    	bactrian.UserService
    }
    
    // This example implementation is using bcrypt. Since bactrian doesn't enforce 
    // this on you, you are free to choose your own implemention of managing users credentials. 
    func (users AuthenticatedUserService) Authenticates(user bactrian.User, password string) bool{
    	return bcrypt.CompareHashAndPassword([]byte(user.Authentication), []byte(password)) == nil
    }
    
    func (AuthenticatedUserService) Get(email string) (bactrian.User, error) {
    	session := connection.NewSession(nil)
   		user := bactrian.User{}
   		if _, err := session.Select("email", "password").From("users").Where("email like ?", email).Load(&user); err == nil {
   			return user, err
   		} else {
   			return bactrian.User{}, err
   		}
   	}
    
