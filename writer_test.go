package bactrian

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

type writer struct {
	gin.ResponseWriter
}

func (w writer) Header() http.Header {
	return http.Header{}
}

func (w writer) WriteHeader(int) {
	return
}

func (w writer) Write(data []byte) (n int, err error) {
	return 0, nil
}
