package bactrian

import (
	"errors"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"strings"
)

type dummyUserService struct{}

var pass = "pass"
var dummyUser = User{"identity", Hash(pass)}

func (dummyUserService) Get(identify string) (User, error) {
	if strings.Compare(dummyUser.Identity, identify) == 0 {
		return dummyUser, nil
	} else {
		return User{}, errors.New("user not found")
	}
}

func (dummyUserService) Authenticates(user User, password string) bool {
	return bcrypt.CompareHashAndPassword([]byte(user.Authentication), []byte(password)) == nil
}

func mockContext() *gin.Context {
	context, _ := gin.CreateTestContext(writer{})
	context.Request = &http.Request{}
	context.Request.Header = http.Header{}
	return context
}
