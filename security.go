package bactrian

import (
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"log"
	"net/http"
)

func Hash(password string) string {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		log.Fatal(err)
	}
	return string(hashedPassword)
}
func denyRequest(c *gin.Context) {
	c.JSON(http.StatusUnauthorized, gin.H{"message": "Not Authorized!"})
	c.Abort()
}
