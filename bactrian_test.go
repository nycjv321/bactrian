package bactrian

import (
	"net/http"
	"testing"
)

var basicAuth = BasicAuth(dummyUserService{})
var context = mockContext()

func TestBasicAuth(t *testing.T) {
	source := []byte(dummyUser.Identity + ":" + pass)
	context.Request.Header.Set("Authorization", "Basic "+base64Encoder.EncodeToString(source))
	basicAuth(context)
	assertEquals(context.Writer.Status(), http.StatusOK, t)
}

func TestBasicAuthFails(t *testing.T) {
	context.Request.Header.Set("Authorization", "Basic "+base64Encoder.EncodeToString([]byte("")))
	basicAuth(context)
	assertEquals(context.Writer.Status(), http.StatusUnauthorized, t)
}

func TestBasicAuthFailsEmptyHeader(t *testing.T) {
	context.Request.Header.Set("Authorization", "")
	basicAuth(context)
	assertEquals(context.Writer.Status(), http.StatusUnauthorized, t)
}

func TestBasicAuthFailsNullHeader(t *testing.T) {
	context.Request.Header.Del("Authorization")
	basicAuth(context)
	assertEquals(context.Writer.Status(), http.StatusUnauthorized, t)
}
