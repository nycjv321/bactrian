package bactrian

import (
	"encoding/base64"
	"github.com/gin-gonic/gin"
	"strings"
)

type User struct {
	Identity       string
	Authentication string
}

type UserService interface {
	Get(identity string) (User, error)
	Authenticates(user User, password string) bool
}

var base64Encoder = base64.StdEncoding

// Performs basic authentication on the given context using
// the user service
func BasicAuth(users UserService) gin.HandlerFunc {
	return func(c *gin.Context) {
		email, pass := extractPrinciple(c)
		if user, err := users.Get(email); err == nil {
			if !users.Authenticates(user, pass) {
				denyRequest(c)
			}
		} else {
			denyRequest(c)
		}
	}
}

func getAuthorizationHeader(c *gin.Context) string {
	authorization := c.GetHeader("Authorization")
	if len(authorization) > 0 {
		return authorization
	} else {
		return ""
	}
}

func getEncodedBasicCredential(c *gin.Context) string {
	return strings.Replace(getAuthorizationHeader(c), "Basic ", "", 1)
}

func decodeAuthorizationHeader(c *gin.Context) string {
	if decodedAuthorizationHeader, err := base64Encoder.DecodeString(getEncodedBasicCredential(c)); err == nil {
		return string(decodedAuthorizationHeader)
	} else {
		return ""
	}
}

func extractPrinciple(c *gin.Context) (email string, pass string) {
	splitHeader := strings.Split(decodeAuthorizationHeader(c), ":")
	if len(splitHeader) == 2 {
		return splitHeader[0], splitHeader[1]
	} else {
		return "", ""
	}

}
